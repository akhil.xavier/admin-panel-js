import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  container: {
    height: window.innerHeight,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  textField: {
    width: 400,
  },
  button: {
    width: 100,
  },
});

export default useStyles;

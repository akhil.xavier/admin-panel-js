import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from 'utils/history';
import Auth from './auth/reducer';

const appReducer = combineReducers({
  router: connectRouter(history),
  Auth,
});

const rootReducer = (stateArg, action) => {
  let state = stateArg;
  if (action.type === 'LOGOUT') {
    state = undefined;
  }
  return appReducer(state, action);
};

export default rootReducer;

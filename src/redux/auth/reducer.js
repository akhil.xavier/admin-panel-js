import produce from 'immer';
import { LOGIN, LOGOUT } from './action';

const initialState = {
  authenticated: false,
};

const reducer = (state = initialState, action) => {
  return produce(state, (draft) => {
    const draftState = draft;
    switch (action.type) {
      case LOGIN:
        draftState.authenticated = true;
        return draftState;
      case LOGOUT:
        draftState.authenticated = false;
        return draftState;
      default:
        return draftState;
    }
  });
};
export default reducer;

export const LOGIN = 'login/LOGIN';
export const LOGOUT = 'login/LOGOUT';

export const loginAction = () => ({
  type: LOGIN,
});

export const logOutAction = () => ({
  type: LOGOUT,
});

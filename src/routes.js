const routes = {
  LOGIN: '/login',
  INBOX: '/inbox',
};

export default routes;

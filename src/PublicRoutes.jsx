import React, { lazy } from 'react';
import { Route, Switch } from 'react-router-dom';

import routes from 'routes';

function App() {
  return (
    <Switch>
      <Route
        exact
        path={routes.LOGIN}
        component={lazy(() => import('pages/Login'))}
      />
      <Route path="/" component={lazy(() => import('components/App'))} />
    </Switch>
  );
}

export default App;

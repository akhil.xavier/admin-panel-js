import React from 'react';
import PropTypes from 'prop-types';

import CircularProgress from '@material-ui/core/CircularProgress';

const height = window.innerHeight;

const Loader = ({ passHeight }) => {
  return (
    <div
      style={{
        height: passHeight || height,
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <CircularProgress disableShrink />
    </div>
  );
};

export default Loader;

Loader.propTypes = {
  passHeight: PropTypes.string,
};

Loader.defaultProps = {
  passHeight: 0,
};

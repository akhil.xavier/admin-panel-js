import React, { lazy, useEffect } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import routes from 'routes';
import SideBar from './SideBar';
import useStyles from './styles';
import 'css/App.css';

function App({ history }) {
  const classes = useStyles();

  const { authenticated } = useSelector((state) => state.Auth);
  useEffect(() => {
    if (!authenticated) {
      history.push('/login');
    }
  }, [authenticated, history]);

  return (
    <>
      <div className={classes.root}>
        <SideBar />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Route exact path="/" component={lazy(() => import('pages/Home'))} />
          <Route
            exact
            path={routes.INBOX}
            component={lazy(() => import('pages/Inbox'))}
          />
        </main>
      </div>
    </>
  );
}

export default withRouter(App);

App.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
};

App.defaultProps = {
  history: PropTypes.object,
};
